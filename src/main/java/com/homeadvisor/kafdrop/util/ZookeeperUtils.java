package com.homeadvisor.kafdrop.util;

/**
 * Created by Arpan Patnaik on 9/28/17.
 */
public class ZookeeperUtils {

    static final String ConsumersPath = "/kafka10/consumers";
    static final String BrokerIdsPath = "/kafka10/brokers/ids";
    static final String BrokerTopicsPath = "/kafka10/brokers/topics";
    static final String TopicConfigPath = "/kafka10/config/topics";
    static final String TopicConfigChangesPath = "/kafka10/config/changes";
    static final String ControllerPath = "/kafka10/controller";
    static final String ControllerEpochPath = "/kafka10/controller_epoch";
    static final String DeleteTopicsPath = "/admin/delete_topics";

    public static String getConsumersPath() {
        return ZookeeperUtils.ConsumersPath;
    }

    public static String getBrokerIdsPath() {
        return ZookeeperUtils.BrokerIdsPath;
    }

    public static String getBrokerTopicsPath() {
        return ZookeeperUtils.BrokerTopicsPath;
    }

    public static String getTopicConfigPath() {
        return ZookeeperUtils.TopicConfigPath;
    }

    public static String getTopicConfigChangesPath() {
        return ZookeeperUtils.TopicConfigChangesPath;
    }

    public static String getControllerPath() {
        return ZookeeperUtils.ControllerPath;
    }

    public static String getControllerEpochPath() {
        return ZookeeperUtils.ControllerEpochPath;
    }

    public static String getDeleteTopicsPath() {
        return ZookeeperUtils.DeleteTopicsPath;
    }

    public static String getTopicsPath(String topic) {
        return BrokerTopicsPath  + "/" + topic;
    }

    public static String getTopicPartitionLeaderAndIsrPath(String topic, int partitionId) {
        return getTopicPartitionPath(topic, partitionId) + "/" + "state";
    }

    public static String getTopicPartitionPath(String topic, int partitionId) {
        return getTopicPartitionsPath(topic) + "/" + partitionId;
    }

    public static String getTopicPartitionsPath(String topic) {
        return getTopicPath(topic) + "/partitions";
    }

    public static String getTopicPath(String topic) {
        return ZookeeperUtils.BrokerTopicsPath + "/" + topic;
    }
}
