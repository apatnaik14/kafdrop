# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The repo is to track modifications made to tool KafDrop for further customizations and in-house usage. The tool will provide JMX support and custom zookeeper support via a cluster and can be
used to track and monitor Kafka state and messages.

### Basic requirements

  Java 8
  Kafka (0.8.1 or 0.8.2 is known to work)
  Zookeeper (3.4.5 or later)

### How do I get set up? ###

Build Package via 
	1. mvn clean package
	
Running with Docker
	1. mvn clean package assembly:single docker:build
	2. docker run -d -p 9000:9000 -e ZOOKEEPER_CONNECT=<host:port,host:port> kafdrop
	
The utility can be accessed via http://localhost:9000.

### Who do I talk to in case of issues? ###

Please feel free to let me know in case of any additions or any specific test cases we can cover to add to the monitoring tool.